import json
import re
import os

os.chdir(os.path.dirname(os.path.abspath(__file__)))


class BuildParts:
    def __init__(self, art_id, prop0, prop1, prop2, prop3, prop4, level, num = 1):
        self.num: str = num
        self.art_id: str = art_id
        self.level: str = level
        self.level = self.level.removeprefix('lv')
        self.prop0: str = prop0
        self.prop1: str = prop1
        if (self.prop1.find(',') == -1): self.prop1 += ',1'
        self.prop2: str = prop2
        if (self.prop2.find(',') == -1): self.prop2 += ',1'
        self.prop3: str = prop3
        if (self.prop3.find(',') == -1): self.prop3 += ',1'
        self.prop4: str = prop4
        if (self.prop4.find(',') == -1): self.prop4 += ',1'

    def dump_str(self):
        return ' '.join(str(value) for value in vars(self).values())

    def __str__(self) -> str:
        return str((self.art_id, self.num, self.prop0, self.prop1, self.prop2, self.prop3, self.prop4, self.level))
    def __iter__(self):
        return iter([self.art_id, self.num, self.prop0, self.prop1, self.prop2, self.prop3, self.prop4, self.level])
    def __repr__(self):
        return self.__str__()

class Character:
    def __init__(self, name: str):
        self.name = name
        self.builds = {}
    def add_builds(self, item_data: dict):
        for item_name, builds in item_data.items():
            processed_builds = []
            for build in builds:
                parts = build.split()
                # locate level
                level = next(part for part in parts if part.startswith("lv"))
                parts.remove(level)

                build_parts = BuildParts(
                    art_id=parts[0],
                    prop0=parts[1],
                    prop1=parts[2],
                    prop2=parts[3],
                    prop3=parts[4],
                    prop4=parts[5],
                    level=level
                ).dump_str()
                processed_builds.append(build_parts)
            self.builds[item_name] = processed_builds


def _load_jsonc(file_path: str) -> dict:
    try:
        with open(file_path, 'r') as f:
            jsonc_content = f.read()
            json_content = re.sub(r'//.*', '', jsonc_content)
            data = json.loads(json_content)
            return data
    except FileNotFoundError:
        print(f"File {file_path} not found.")
        return {}
    except json.JSONDecodeError:
        print(f"Failed to decode JSON from {file_path}.")
        return {}

def _load_raw(builds_raw_json: dict) -> list[Character]:
    characters = []

    for character_name, items in builds_raw_json.items():
        char_instance = Character(character_name)
        char_instance.add_builds(items)
        characters.append(char_instance)
        
    return characters

def _dump_jsonc(chars: list[Character], file_path: str):
    with open(file_path, 'w') as f:
        json_obj = {}
        for char in chars:
            json_obj[char.name] = char.builds
        json.dump(json_obj, f)

def _PRINT_TEST(chars: list[Character]):
    for i in range(1, 2):
        for _name, _list in chars[i].builds.items():
            for _str in _list:
                print(_str)
                print()
        # OR
        print(chars[i].name + ":" + str(chars[i].builds))
        print()

def main():
    builds_raw_json = _load_jsonc('./data/builds_raw.jsonc')
    chars = _load_raw(builds_raw_json)
    _dump_jsonc(chars, './data/builds.jsonc')


if __name__ == '__main__':
    main()
