import json
import string
import urllib.parse
import requests
import random
import hashlib
from flask import Response

import os; os.chdir(os.path.dirname(os.path.abspath(__file__)))

'''EXT'''
# response.py
def json_rsp(code, data):
   return Response(json.dumps({"retcode": code} | data, separators=(',', ':')), mimetype='application/json')

def json_rsp_with_msg(code, msg, data):
   return Response(json.dumps({"retcode": code, "message": msg} | data, separators=(',', ':')), mimetype='application/json')

# define.py
RES_SUCCESS = 200
RES_BAD_REQUEST = 400
RES_UNKNOWN_REGION = 403
RES_PROXY_ERR = 502

# crypto.py
def sha256_sign(secret, message):
    sha256 = hashlib.sha256()
    sha256.update(f"{message}{secret}".encode())
    return sha256.hexdigest()

'''EXT'''

TICKET_LEN_HARDCODE = 32
HOST_HARDCODE = 'http://192.168.200.130:22541'
SIGN_HARDCODE = ''
REGION_HARDCODE = '1_6_live'
COMMAND_HARDCODE = '1127'  # addItem

def send_cmd(payload) -> dict:
    if payload == "":
        return {}

    payload = json.dumps(payload)

    flask_response = _send_cmd(payload=payload)
    resp = json.loads(flask_response.json['response'])
    # resp = json.loads('{"data":null,"msg":"recv from nodeserver timeout","retcode":0,"ticket":"ydFPjRqplYFDFjIaYIukhehjKfCHRftz"}\n')

    if resp['retcode'] != 0:
        raise Exception(f"retcoded: {resp['retcode']}, msg: {resp['msg']}, data: {resp['data']}")
    else:
        return resp

# from routes.py
def _send_cmd(payload: dict) -> Response:
    if REGION_HARDCODE is None:
        print(f"Unregistered region={REGION_HARDCODE}")
        return json_rsp_with_msg(RES_UNKNOWN_REGION, f"Unregistered region", {"region": REGION_HARDCODE})

    try:
        kvs = [
            f"cmd={COMMAND_HARDCODE}",
            f"ticket={''.join(random.choice(string.ascii_letters) for i in range(TICKET_LEN_HARDCODE))}"
        ]
        if REGION_HARDCODE:
            kvs.append(f"region={REGION_HARDCODE}")
        for kv in json.loads(payload).items():
            kvs.append(f"{kv[0]}={kv[1]}")
        kvs.sort()

        qstr = f"{'&'.join(kvs)}"
        sign = sha256_sign(SIGN_HARDCODE, qstr)
    except Exception as err:
        print(f"Unexpected {err=}, {type(err)=} while preparing query string")
        return json_rsp_with_msg(RES_BAD_REQUEST, f"{err=}, {type(err)=}", {})

    try:
        res = requests.get(
            f"{HOST_HARDCODE}/api?{urllib.parse.quote_plus(qstr, safe='=&')}&sign={sign}").content
        return json_rsp(RES_SUCCESS, {"response": res.decode()})
    except Exception as err:
        print(f"Unexpected {err=}, {type(err)=} while forwarding request")
        return json_rsp_with_msg(RES_PROXY_ERR, f"{err=}, {type(err)=}", {})

def cmd(uid: int, item_count: int, item_id: int, extra_params: dict[str, int]) -> dict:
    return {
        "uid": uid,
        "item_count": item_count,
        "item_id": item_id,
        "extra_params": extra_params,
    }

def main():
    pass

if __name__ == '__main__':
    main()
