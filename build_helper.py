import os
import click
import json
import time
import re
from prompt_toolkit import PromptSession
from prompt_toolkit.history import InMemoryHistory
from prompt_toolkit.formatted_text import FormattedText
from prompt_toolkit.completion import NestedCompleter
from cmd_exec import *

os.chdir(os.path.dirname(os.path.abspath(__file__)))

BUILD_JSON_FILE_PATH = './data/builds.jsonc'

_USAGE = """
👀 Usage: 
1. set: set uid, example: `set 10001`.
2. type a character name, and follow tips. 
   final you will get work by something like `Ayato echoes`
"""
_CHARACTERS: dict[str, dict[str, list]]

def click_print(*args, **kwargs):
    click.echo(click.style(*args, **kwargs))

def _load_jsonc(file_path: str) -> dict:
    try:
        with open(file_path, 'r') as f:
            jsonc_content = f.read()
            json_content = re.sub(r'//.*', '', jsonc_content)
            data = json.loads(json_content)
            return data
    except FileNotFoundError:
        print(f"File {file_path} not found.")
        return {}
    except json.JSONDecodeError:
        print(f"Failed to decode JSON from {file_path}.")
        return {}

def build(name, suit = None, uid = None):
    if name not in _CHARACTERS:
        click_print(f"❌ Error: No such character '{name}' found.", fg='red')
        return
    
    if suit is None:
        builds = ', '.join(_CHARACTERS[name].keys())
        click_print(f"Available builds: {builds}")
    elif suit in _CHARACTERS[name]:
        click_print(f"⚙️ Building character {name} version {suit}... ", fg='yellow')
        # BUILD
        try:
            for artifact in _CHARACTERS[name][suit]:
                CMD = 'equip add '+artifact
                click_print(f"🚀 Sending `{CMD}`...", fg='blue')
    
                parts = artifact.split()
                assert(len(parts) == 8)
                
                item_count, item_id, level, mainPropId = map(int, parts[:4])
                appendPropIdList = []
                for part in parts[4:]:
                    if ',' in part:
                        val_str, num_str = part.split(',')
                        num = int(num_str)
                        val = int(val_str)
                        appendPropIdList.extend([val] * num)
                    else:
                        appendPropIdList.append(int(part))         
    
                extra_params = {}
                extra_params['level'] = level + 1  # real level have to be add one
                extra_params['mainPropId'] = mainPropId
                extra_params['appendPropIdList'] = appendPropIdList
                extra_params = json.dumps(extra_params)
    
                resp = send_cmd(cmd(uid=uid, item_count=item_count, item_id=item_id,
                                    extra_params=extra_params))
                # &uid=160217663
                # &item_id=59524
                # &extra_params={"mainPropId":12001,"appendPropIdList":[201021,501064,501064,501064],"level":21}
                # &item_count=1
    
                click_print(f"🪄  retcoded: {resp['retcode']}, msg: {resp['msg']}", fg='white')
    
                time.sleep(0.5)
        except Exception as e:
            pass
            print(f"Exception: [{e}]")
    else:
        click_print(f"❌ Error: No such character/suit combination found. ", fg='red')


def cli():
    global _USAGE
    global _CHARACTERS
    _CHARACTERS = _load_jsonc(BUILD_JSON_FILE_PATH)
    
    UID = 160217663

    session = PromptSession(history=InMemoryHistory())

    click_print('😇 Welcome to the character build CLI! 😊', fg='blue')

    completer_dict = {key: {art_name: None for art_name in value.keys()} for key, value in _CHARACTERS.items()}
    completer_dict.update({'exit': None, 'quit': None, 'q': None, 'set': None, 's': None})

    completer = NestedCompleter.from_nested_dict(completer_dict)

    if (UID is not None):
        click_print(f"👉 Now uid is hardcode: {UID}", fg='green')

    while True:
        if (UID is None or UID == ""):
            click_print(f"👉 Now uid is: {UID} Set uid by `set xxx` ⌨️", fg='yellow')

        input_str = session.prompt(FormattedText([('ansicyan', '⌨️  Please enter a command: ')]),
                                   completer=completer)
        args = input_str.split()

        if args[0] in ['exit', 'quit', 'q']:
            break
        elif len(args) == 2:
            if args[0] in ['set']:
                UID = args[1]
                click_print(f"👉 Now uid is: {UID}", fg='green')
                continue
            else:
                build(*args, uid=UID)
        else:
            click_print(f"🥴 Invalid input. ", fg='red')
            click_print(_USAGE, fg='white')

    click_print(f"\nBye. 🍾\n", fg='green')


if __name__ == '__main__':
    cli()
