Mess codes, use at your own risk.

For official leak use, tested on version 1.6.

some codes/docs copy from: 
- [gio-docker-braindead](https://archive.org/download/gio-docker-braindead/Prerequisites%20%26%20Tools.7z)
- [CharacterBuilder](https://github.com/Penelopeep/CharacterBuilder)
- [Handbook](https://gitlab.com/YuukiPS/GC-Data/-/tree/main/GM%20Handbook)

run `python build_helper.py`.
